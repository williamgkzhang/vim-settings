local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()


return require('packer').startup({function(use)
	-- Packer can manage itself
	use 'wbthomason/packer.nvim'

	use 'nvim-tree/nvim-tree.lua'

	-- Fun devicons so fun
	use 'nvim-tree/nvim-web-devicons'

	use 'RRethy/nvim-base16'
	use "rebelot/kanagawa.nvim"

	-- Statusline
	use { 'nvim-lualine/lualine.nvim', requires = { 'nvim-tree/nvim-web-devicons', 'RRethy/nvim-base16'}, }

	-- Automatic commenting
	use 'scrooloose/nerdcommenter'

	-- Editor Config
	use 'editorconfig/editorconfig-vim'

	-- Git wrapper
	use 'tpope/vim-fugitive'

	-- Mappings to manipulate surroundings
	-- use 'tpope/vim-surround'

	-- Create Command Aliases
	-- use 'vim-scripts/cmdalias.vim'

	-- Live replace previews
	-- use 'osyo-manga/vim-over', { 'on': 'OverCommandLine' }

	-- Delete buffers except current
	-- use 'schickling/vim-bufonly', { 'on': 'BufOnly' }

	-- Lua helper library
	use 'nvim-lua/plenary.nvim'

	-- Gutter git status
	use { 'lewis6991/gitsigns.nvim', requires = {'nvim-lua/plenary.nvim'} }

	-- Code completion
	-- use {'neoclide/coc.nvim', branch = 'release'}

	-- Indentation markers
	--use 'Yggdroot/indentLine'
	use 'lukas-reineke/indent-blankline.nvim'

	-- Fuzzy file searching
	use 'nvim-telescope/telescope.nvim'

	-- AST languages
	use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}

	use { "rafcamlet/tabline-framework.nvim",  requires = "nvim-tree/nvim-web-devicons" }


	-- LSP
	use {
		'VonHeikemen/lsp-zero.nvim',
		requires = {
			-- LSP Support
			{'neovim/nvim-lspconfig'},
			{'williamboman/mason.nvim'},
			{'williamboman/mason-lspconfig.nvim'},

			-- Autocompletion
			{'hrsh7th/nvim-cmp'},
			{'hrsh7th/cmp-buffer'},
			{'hrsh7th/cmp-path'},
			{'saadparwaiz1/cmp_luasnip'},
			{'hrsh7th/cmp-nvim-lsp'},
			{'hrsh7th/cmp-nvim-lua'},

			-- Snippets
			{'L3MON4D3/LuaSnip'},
			{'rafamadriz/friendly-snippets'},
		}
	}

	if packer_bootstrap then
		require('packer').sync()
	end
end,
config = {
  display = {
    open_fn = require('packer.util').float,
  }
}})


