" coc.vim maps
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"


" Edit vim-settings
nnoremap <leader>rc :e $SETTINGS<cr>

" Source vimrc
nnoremap <leader>rC :so $MYVIMRC<cr>

