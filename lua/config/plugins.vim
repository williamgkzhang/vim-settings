""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General Plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Lightline bottom bar"
Plug 'itchyny/lightline.vim'

" Filesystem explorer
" Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'kyazdani42/nvim-tree.lua'

" Automatic commenting
Plug 'scrooloose/nerdcommenter'

" Editor Config
Plug 'editorconfig/editorconfig-vim'

" Git wrapper
Plug 'tpope/vim-fugitive'

" Mappings to manipulate surroundings
Plug 'tpope/vim-surround'

" Create Command Aliases
Plug 'vim-scripts/cmdalias.vim'

" Live replace previews
Plug 'osyo-manga/vim-over', { 'on': 'OverCommandLine' }

" Delete buffers except current
Plug 'schickling/vim-bufonly', { 'on': 'BufOnly' }

" Gutter git status
Plug 'airblade/vim-gitgutter'

" Code completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Indentation markers
"Plug 'Yggdroot/indentLine'
Plug 'lukas-reineke/indent-blankline.nvim'


" Fun devicons so fun
Plug 'kyazdani42/nvim-web-devicons'

" Fuzzy file searching
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Language Plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" All the language polyglot
" Plug 'sheerun/vim-polyglot'

" Javascript libraries
Plug 'othree/javascript-libraries-syntax.vim', { 'for': ['javascript', 'javascript.jsx'] }

" Styled components
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }

" Styled components
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Color plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plug 'chriskempson/base16-vim'
Plug 'RRethy/nvim-base16'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" End of plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Call plug end
call plug#end()
