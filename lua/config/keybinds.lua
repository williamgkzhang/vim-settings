vim.g.mapleader = " "

-- Toggle Nerdtree	
vim.keymap.set('', '<Leader>n', ':NvimTreeToggle<CR>', { noremap = true})
vim.keymap.set('', '<Leader>m', ':NvimTreeFindFile<CR>', { noremap = true})


vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)


local builtin = require('telescope.builtin')

vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
vim.keymap.set("n", "<leader>fg", builtin.live_grep, {})
vim.keymap.set("n", "<leader>fc", builtin.colorscheme, {})
vim.keymap.set("n", "<leader>fr", builtin.resume, {})
vim.keymap.set("n", "<leader>fu", builtin.lsp_references, {})

vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")


vim.cmd(string.format("source %s/lua/config/keybinds.vim", vim.fn.stdpath('config')))
