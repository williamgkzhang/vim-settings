local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)



-- return require('packer').startup({function(use)
require("lazy").setup({

	{'RRethy/nvim-base16', lazy = false, priority=1000, config = function()
	end},
	{'nvim-tree/nvim-tree.lua', lazy = false},

	-- Fun devicons so fun
	'nvim-tree/nvim-web-devicons',

	-- {"rebelot/kanagawa.nvim", lazy= false},

	-- Statusline
	{ 'nvim-lualine/lualine.nvim', dependencies = { 'nvim-tree/nvim-web-devicons', 'RRethy/nvim-base16'}, },

	-- Automatic commenting
	'scrooloose/nerdcommenter',

	-- Editor Config
	'editorconfig/editorconfig-vim',

	-- Git wrapper
	'tpope/vim-fugitive',

	-- Mappings to manipulate surroundings
	-- use 'tpope/vim-surround'

	-- Create Command Aliases
	-- use 'vim-scripts/cmdalias.vim'

	-- Live replace previews
	-- use 'osyo-manga/vim-over', { 'on': 'OverCommandLine' }

	-- Delete buffers except current
	-- use 'schickling/vim-bufonly', { 'on': 'BufOnly' }

	-- Lua helper library
	'nvim-lua/plenary.nvim',

	-- Gutter git status
	{ 'lewis6991/gitsigns.nvim', dependencies = {'nvim-lua/plenary.nvim'} },

	-- Code completion
	-- use {'neoclide/coc.nvim', branch = 'release'}

	-- Indentation markers
	--use 'Yggdroot/indentLine'
	'lukas-reineke/indent-blankline.nvim',

	-- Fuzzy file searching
	'nvim-telescope/telescope.nvim',

	-- AST languages
	{ 'nvim-treesitter/nvim-treesitter', build = ':TSUpdate'},

	{ "rafcamlet/tabline-framework.nvim",  dependencies = {"nvim-tree/nvim-web-devicons" }},


	-- LSP
	 {
		'VonHeikemen/lsp-zero.nvim',
		dependencies = {
			-- LSP Support
			{'neovim/nvim-lspconfig'},
			{'williamboman/mason.nvim'},
			{'williamboman/mason-lspconfig.nvim'},

			-- Autocompletion
			{'hrsh7th/nvim-cmp'},
			{'hrsh7th/cmp-buffer'},
			{'hrsh7th/cmp-path'},
			{'hrsh7th/cmp-nvim-lsp'},
			{'hrsh7th/cmp-nvim-lua'},
			{'L3MON4D3/LuaSnip'},
		}
	}
})

