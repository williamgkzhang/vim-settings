--------------------------------------------------------------------------------
-- Interface Settings
--------------------------------------------------------------------------------

-- Enable Neovim full color
--vim.opt.termguicolors = true

-- vim.cmd('silent! colorscheme base16-brewer')
-- vim.cmd('silent! colorscheme base16-kanagawa')
-- vim.cmd('silent! colorscheme kanagawa')
--vim.opt.background = 'dark'
vim.opt.guifont = 'HackNF:h18'

vim.cmd([[hi CocMenuSel guibg=#2a2b2c]])
vim.cmd([[hi LineNr guibg=none guifg=#888888]])

-- Remove escape lag
vim.opt.timeoutlen=1000 
vim.opt.ttimeoutlen=0


--------------------------------------------------------------------------------
-- General Settings
--------------------------------------------------------------------------------

-- Variables can be set per file
vim.opt.modeline = true

-- Show line numbers
vim.opt.number = true

-- Highlights matching braces
vim.opt.showmatch = true

-- Search starts while entering string
vim.opt.incsearch = true

-- Search highlighting
vim.opt.hlsearch = true

-- Search ignore case
vim.opt.ignorecase = true

-- Search ignore case unless search contains an uppercase
vim.opt.smartcase = true

-- Autoloads changes to file made outside Vim
vim.opt.autoread = true

-- Do not wrap lines
vim.opt.wrap = false

-- Prevent automatically inserting line breaks
vim.opt.textwidth = 0

-- Command line completion help
vim.opt.wildmenu = true

-- Extensions to ignore in searches
vim.opt.wildignore:append{"*.o", "*.~", "*.pyc", "*.exe", "*/tmp/*", "*.so", "*.swp", "*.zip", "*\\tmp\\*"}

-- Turn off folding by default
vim.opt.foldenable = false

-- Fold by syntax highlighting
vim.opt.foldmethod = "syntax"

-- Minimum number of screen rows above and below the cursor
vim.opt.scrolloff = 4

-- Minimum number of screen columns to the left and right of the cursor
vim.opt.sidescrolloff = 0

-- Always show the status line
vim.opt.laststatus = 2

-- Enable mouse in all modes
vim.opt.mouse = 'a'

-- Allow use of system clipboard
vim.opt.clipboard = "unnamed"

-- Show replace preview
vim.opt.inccommand = "nosplit"

-- Buffer screen updates to speed up macros etc.
vim.opt.lazyredraw = true

-- Disable message at start
vim.opt.shortmess:append({I = true})

-- New window appears below current
vim.opt.splitbelow = true

-- New window appears to the right of current
vim.opt.splitright = true

-- Set charater representation via UTF-8
vim.opt.encoding = "utf8"

-- File type preferences
vim.opt.fileformats = {"unix", "dos"}

-- Don't create backups
vim.opt.backup = false
vim.opt.wb = false

-- Turn off swapfiles
vim.opt.swapfile = false

-- Buffer becomes hidden when abandoned to prevent need to save
vim.opt.hidden = true

-- Enable syntax highlighting
vim.cmd("syntax on")

-- Set searching to global by default
vim.opt.gdefault = true

-- Make backspace behave
vim.opt.backspace = {"indent", "eol", "start"}

-- Turn on lua filetypes
vim.g.do_filetype_lua = 1  
vim.g.did_load_filetypes = 0


--------------------------------------------------------------------------------
-- Indent Settings
--------------------------------------------------------------------------------

-- Automatically indent properly
vim.opt.autoindent = true
vim.opt.smartindent = true

-- Represent 1 tab as 2 columns
vim.opt.tabstop = 2

-- Text is indented by 2 columns
vim.opt.shiftwidth = 2

-- Insert 2 columns when tab pressed
vim.opt.softtabstop = 2
