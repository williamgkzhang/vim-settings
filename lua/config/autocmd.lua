vim.api.nvim_create_autocmd({"BufNewFile", "BufRead"}, {
  pattern = {"Jenkinsfile"},
  command = "set filetype=groovy",
})


-- Fix for reloading files when changed outside neovim
-- autocmd BufEnter,FocusGained * checktime
vim.api.nvim_create_autocmd({"BufEnter", "FocusGained"}, {
  pattern = {"*"},
  command = "checktime",
})

vim.api.nvim_create_autocmd("BufWritePre", {
		desc="Auto format on save",
		pattern="<buffer>",
		callback = function()
				vim.lsp.buf.format()
		end
})


--[[
" OLD STUFF
" Markdown
" autocmd BufNewFile,BufReadPost *.md set filetype=markdown

" Markdown indent settings
" autocmd FileType markdown setlocal tabstop=4 shiftwidth=4 softtabstop=4

" RVM .ruby-env
"autocmd BufNewFile,BufReadPost *.ruby-env set filetype=sh

" JSON rc files
"autocmd BufNewFile,BufReadPost .babelrc,.stylelintrc set filetype=json

" Fix for reloading files when changed outside neovim
"autocmd BufEnter,FocusGained * checktime

"autocmd BufReadPre *.js let b:javascript_lib_use_react = 1

"autocmd BufReadPre *.ts,*.tsx set formatoptions+=ro
]]--
