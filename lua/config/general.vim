""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Interface Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable Neovim full color
"set termguicolors

" Colorscheme
"silent! colorscheme base16-brewer

" set background=dark

" Font
"set guifont=HackNF:h18

" Hide scrollbars
"set guioptions-=r
"set guioptions-=R
"set guioptions-=l
"set guioptions-=L

" Hide menubar
"set guioptions-=m

" Hide toolbar
"set guioptions-=T

" Set block/vertical bar cusor correctly in iTerm
"let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1

" Remove esc lag
"set timeoutlen=1000 ttimeoutlen=0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable Vim
"set nocompatible
"
" Variables can be set per file
"set modeline
"
" Show line numbers
"set number
"
" Highlights matching braces
"set showmatch
"
" Search starts while entering string
"set incsearch
"
" Search highlighting
"set hlsearch
"
" Search ignore case
"set ignorecase
"
" Search ignore case unless search contains an uppercase
"set smartcase
"
" Autoloads changes to file made outside Vim
"set autoread
"
" Do not wrap lines
"set nowrap
"
" Prevent automatically inserting line breaks
"set textwidth=0
"
" Command line completion help
"set wildmenu

" Extensions to ignore in searches
"set wildignore+=*.o,*.~,*.pyc,*.exe,*/tmp/*,*.so,*.swp,*.zip,*\\tmp\\*

" Turn off folding by default
"set nofoldenable

" Fold by syntax highlighting
"set foldmethod=syntax

" Minimum number of screen rows above and below the cursor
"set scrolloff=4

" Minimum number of screen columns to the left and right of the cursor
"set sidescrolloff=0

" Always show the status line
"set laststatus=2

" Enable mouse in all modes
"set mouse=a

" Allow use of system clipboard
" set clipboard=unnamed

" Buffer screen updates to speed up macros etc.
"set lazyredraw

" Disable message at start
"set shortmess+=I

" New window appears below current
"set splitbelow

" New window appears to the right of current
"set splitright

" Set charater representation via UTF-8
"silent! set encoding=utf8

" File type preferences
"set fileformats=unix,dos

" Don't create backups
"set nobackup
"set nowb

" Turn off swapfiles
"set noswapfile

" Buffer becomes hidden when abandoned to prevent need to save
"set hidden

" Enable syntax highlighting
"syntax on

" Enable filetype plugins
"filetype plugin on

" Enable filetype indenting
"filetype indent on

" Set searching to global by default
"set gdefault

" Make backspace behave
"set backspace=indent,eol,start

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Indent Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Automatically indent properly
"set autoindent

" Represent 1 tab as 2 columns
"set tabstop=2

" Text is indented by 2 columns
"set shiftwidth=2

" Insert 2 columns when tab pressed
"set softtabstop=2


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Indent Switch
"
" Helps switch between space and tab indentation
"
" <leader>it        - toggle indentation method
" <leader>i<tab>    - switch to tab indentation
" <leader>i<space>  - switch to space indentation
" <leader>is        - show current indentation method
" <leader>ir        - reindent the file using current indentation method
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"if !exists("g:indentswitch_indent")
"  let g:indentswitch_indent = "spaces"
"endif
"
"function! s:Ispaces()
"  let g:indentswitch_indent = "spaces"
"  set expandtab
"  set smarttab
"  echo "Space Indentation"
"endfunction
"
"function! s:Itabs()
"  let g:indentswitch_indent = "tabs"
"  set noexpandtab
"  set nosmarttab
"  echo "Tab Indentation"
"endfunction
"
"function! s:Itoggle()
"  if g:indentswitch_indent == "tabs"
"    call s:Ispaces()
"  else
"    call s:Itabs()
"  endif
"endfunction
"
"function! s:Ishow()
"  if g:indentswitch_indent == "tabs"
"    echo "Tab Indentation"
"  else
"    echo "Space Indentation"
"  endif
"endfunction
"
"if g:indentswitch_indent == "tabs"
"  set noexpandtab
"  set nosmarttab
"else
"  set expandtab
"  set smarttab
"endif
