local function my_on_attach(bufnr)
  local api = require "nvim-tree.api"

  local function opts(desc)
    return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
  end

  -- default mappings
  api.config.mappings.default_on_attach(bufnr)

  -- custom mappings
  vim.keymap.set('n', 'u', api.tree.change_root_to_parent,  opts('Up'))
  --vim.keymap.set('n', '', api.fs.rename_full,              opts('Rename: Full Path'))
  vim.keymap.set('n', '?',     api.tree.toggle_help,        opts('Help'))
end 

require'nvim-web-devicons'.setup {
 -- globally enable default icons (default to false)
 -- will get overriden by `get_icons` option
 default = true;
}

require("nvim-tree").setup({
  disable_netrw = true,
  hijack_netrw = true,
	on_attach = my_on_attach,

  view = {
    adaptive_size = true,
		--[[
    mappings = {
      list = {
        { key = "u", action = "dir_up" },
      },
    },
		]]
  },
  renderer = { icons = {
      glyphs = {
        git = {
          unstaged = "*",
          staged = "+",
          untracked = "?"
        }
      }
    }
  }
})
