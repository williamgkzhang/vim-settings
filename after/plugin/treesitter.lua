require'nvim-treesitter.configs'.setup{
	ensure_installed = {
		"bash",
		"css",
		"dockerfile",
		"fish",
		"go",
		"graphql",
		"html",
		"javascript",
		"jsdoc",
		"json",
		"lua",
		"markdown",
		"python",
		"ruby",
		"scss",
		"sql",
		"toml",
		"tsx",
		"typescript",
		"vim",
		"yaml"
	},
	sync_install = false,
	auto_install = true,
  highlight =  {
    enable = true,
  },
	indent = { enable = true },
}
